# Spring AMPQ with RabbitMQ part 2 #

## Introduction ##
Before I will show you and explain some other details of working with RabbitMQ, I highly recommend to review my first part(demo setup from part 1 will be used also in here) at:
```
https://bitbucket.org/tomask79/spring-rabbit-mq
```
which shows how to install RabbitMQ or what is AMPQ compare to JMS. In this repo we will take a look at advanced technique of receiving 
messages via MessageListener containers and MessageListenerAdapters. Also I promised you introduction of acknowledges modes in Spring AMPQ.
As well as really important is to understand how message converters works in AMPQ. So let's begin.

## MessageListener containers ##
Last time we made very simple receiver via @RabbitListener and @RabbitHandler annotations: 

```
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

/**
 * @author Tomas Kloucek
 */
@RabbitListener(queues = {"fruits", "vegetables"})
public class RabbitMqReceiver {

	@RabbitHandler
	public void receive(String in) {
		System.out.println(" [x] Received '" + in + "'");
	}

}
```
which works fine. *When using **@RabbitListener** annotation at class level one **SimpleMessageListenerContainer** is created according the **RabbitListenerContainerFactory** to server all methods annotated with **@RabbitHandler** annotation*. Another possibility is to define our SimpleMessageListenerContainer manually, so let's do it.

## MessageListener interface and Message converters ##

First we need to define Spring AMPQ **MessageListener** interface with onMessage method where we're going to be receiving AMPQ **Message** objects. If you don't want to be tided to MessageListener interface you can use **MessageListenerAdapter** with your own method. Payload is going to be extracted by default via **SimpleMessageConverter**

```
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

public class RabbitMqReceiver implements MessageListener {

	@Override
	public void onMessage(Message message) {
		System.out.println("Content type: "+message.getMessageProperties().getContentType());
		System.out.println("Payload : "+new String(message.getBody()));
		System.out.println("Received exchange: "+
		message.getMessageProperties().getReceivedExchange());
		System.out.println("----------------");
	}
}
```

### SimpleMessageConverter ###

Is the default message converter which is going to be used by MessageListenerAdapter to extract payload of your messages and injecting it into your receiving methods. *When **converter extracts payload from AMPQ Message** it first **checks content_type Message property**. If it begins with "text" like "text/plain" **payload of byte[] data will be converted into String**. If it is "application/x-java-serialized-object" then converter tries to convert to Java Object.* Anyway in the demo we won't be using any converter, I wan't to show you how to print whole Message properties. So let's define our SimpleMessageListenerContainer.

```
    @Bean
	MessageListener listener() {
		final RabbitMqReceiver adapt = new RabbitMqReceiver();
		return adapt;
	}
	
	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory, 
			RabbitMqReceiver listener) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames("fruits", "vegetables");
		container.setMessageListener(listener);
		return container;
	}
```
*MessageListenerContainer defined like this will be in **AUTO** acknowledge mode.* Let's take a look at all of them:

* AUTO - Message will be acknowledged when receiving method ends successfully.
* MANUAL - With this mode you are responsible for acknowledging using Channel.basicAck(long, boolean) method. To be able to do this our RabbitMqReceiver class would need to implement **ChannelAwareMessageListener**.
* NONE - All messages acknowledged as soon as they're sent.

### AUTO acknowledge Mode ###

AUTO mode is the most common usage. *For example, what will happen if our **RabbitMqReceiver.onMessage method would throw an Exception**. **The answer is that message will be re-queued back**, you can affect this behaviour with **SimpleMessageListenerContainer.setDefaultRequeueRejected** method*. Btw Spring AMPQ doesn't offer setting for number of attempts to process when message is rejected. You need to implement this with your own code, like:

When requeuing Message, you can increment some custom counter Message property, when Message is redelivered. After appropriate count, you can publish it into dead letter queue.

### Best Effort transaction synchronization, does it work with AMPQ? ###

Yes, thank god! Simply set **SimpleMessageListenerContainer.channelTransacted** flag to true and your message receiving will be synchronized with outside PlatformTransactionManager. **Btw this transaction mode can be used for example with AUTO acknowledge mode**. Broker will simply be requesting AMPQ transaction commit in addition to acknowledgement. This is difference to JMS where either akcnowledging or local transaction can be used.

# Demo test #

Simply clone this repo and run:

```
mvn clean install
```
after that, run the demo, which will send three messages into fruits and vegetables queues. Messages will be then retrieved via our SimpleMessageListenerContainer.

```
java -jar target/demo-0.0.1-SNAPSHOT.jar
```
Output should be:

```
Content type: text/plain
Payload : Orange
Received exchange: test_exchange
----------------
Content type: text/plain
Payload : xxx1
Received exchange: test_exchange
----------------
Content type: text/plain
Payload : Banana
Received exchange: test_exchange
----------------
Content type: text/plain
Payload : xxx2
Received exchange: test_exchange
----------------
Content type: text/plain
Payload : Appricotes
Received exchange: test_exchange
----------------
Content type: text/plain
Payload : xxx3
Received exchange: test_exchange
----------------
```

that's all, thanks for following. I plan part three where I will take a look at request/response pattern with rabbitmq. See ya

Tomas