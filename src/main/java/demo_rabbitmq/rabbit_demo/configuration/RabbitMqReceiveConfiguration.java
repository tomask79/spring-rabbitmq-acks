package demo_rabbitmq.rabbit_demo.configuration;

import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo_rabbitmq.rabbit_demo.receiver.RabbitMqReceiver;

@Configuration
public class RabbitMqReceiveConfiguration {
	
	{
		System.out.println("Creating receiving configuration.");
	}
	
	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = 
				new CachingConnectionFactory("localhost");
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		return connectionFactory;
	}
		
	@Bean
	public Queue fruits() {
		return new Queue("fruits");
	}
	
	@Bean
	public Queue vegetables() {
		return new Queue("vegetables");
	}
	
    @Bean
    RabbitMqReceiver receiver() {
        return new RabbitMqReceiver();
    }
    
    @Bean
	public RabbitTemplate rabbitTemplate() {
		RabbitTemplate template = new RabbitTemplate(connectionFactory());
		template.setExchange("test_exchange");
		return template;
	}
	
	@Bean
	MessageListener listener() {
		final RabbitMqReceiver adapt = new RabbitMqReceiver();
		return adapt;
	}
	
	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory, 
			RabbitMqReceiver listener) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames("fruits", "vegetables");
		container.setMessageListener(listener);
		return container;
	}
}
