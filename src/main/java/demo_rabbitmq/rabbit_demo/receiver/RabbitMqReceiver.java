package demo_rabbitmq.rabbit_demo.receiver;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

public class RabbitMqReceiver implements MessageListener {

	@Override
	public void onMessage(Message message) {
		System.out.println("Content type: "+message.getMessageProperties().getContentType());
		System.out.println("Payload : "+new String(message.getBody()));
		System.out.println("Received exchange: "+
		message.getMessageProperties().getReceivedExchange());
		System.out.println("----------------");
	}
}
