package demo_rabbitmq.rabbit_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import demo_rabbitmq.rabbit_demo.sender.RabbitMqSender;

/**
 * Demo of rabbitmq
 */
@SpringBootApplication
public class App 
{
	@Bean(initMethod="send")
	public RabbitMqSender sender() {
		final RabbitMqSender sender = new RabbitMqSender();
		return sender;
	}
	
    public static void main(String[] args) throws Exception {
        SpringApplication.run(App.class, args);
    }
}
