package demo_rabbitmq.rabbit_demo.sender;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Tomas Kloucek
 */
public class RabbitMqSender {

	@Autowired
	private RabbitTemplate template;
	
	public void send() {
		// fruits
		this.template.convertAndSend("fruits", "Orange");
		this.template.convertAndSend("fruits", "Banana");
		this.template.convertAndSend("fruits", "Appricotes");
		// vegetables
		this.template.convertAndSend("vegetables", "xxx1");
		this.template.convertAndSend("vegetables", "xxx2");
		this.template.convertAndSend("vegetables", "xxx3");
	}
}

